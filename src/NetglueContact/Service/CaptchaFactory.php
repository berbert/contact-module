<?php
/**
 * Factory class for returning a Captch from config spec to
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @package	Netglue_ContactModule
 * @link https://bitbucket.org/netglue/zf2-contact-module
 */

namespace NetglueContact\Service;

use Traversable;
use Zend\Captcha\Factory as ZendCaptchaFactory;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\ArrayUtils;

/**
 * Factory class for returning a Captch from config spec to
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @package	Netglue_ContactModule
 * @link https://bitbucket.org/netglue/zf2-contact-module
 */
class CaptchaFactory implements FactoryInterface {
	
	/**
	 * Implement Factory Interface
	 * @param ServiceLocatorInterface $services
	 * @return Zend\Captcha\AdapterInterface
	 */
	public function createService(ServiceLocatorInterface $services) {
		$config  = $services->get('config');
		if($config instanceof Traversable) {
			$config = ArrayUtils::iteratorToArray($config);
		}
		$spec = $config['netglue_contact']['form']['captcha'];
		$captcha = ZendCaptchaFactory::factory($spec);
		return $captcha;
	}
	
}
