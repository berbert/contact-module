<?php
/**
 * Netglue Contact Module Observer Class
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @package	Netglue_ContactModule
 * @link https://bitbucket.org/netglue/zf2-contact-module
 */

namespace NetglueContact\Service;

use Zend\EventManager\EventInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\FactoryInterface;

/**
 * Netglue Contact Module Observer Class
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @package	Netglue_ContactModule
 * @link https://bitbucket.org/netglue/zf2-contact-module
 */
class ContactResponder implements FactoryInterface, ServiceLocatorAwareInterface {
	
	/**
	 * Service Locator
	 * @var ServiceLocatorInterface
	 */
	protected $serviceLocator;
	
	
	/**
	 * Set Service Locator
	 * @implements \Zend\ServiceManager\ServiceLocatorAwareInterface
	 * @return void
	 * @param ServiceLocatorInterface $serviceLocator
	 */
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
		$this->serviceLocator = $serviceLocator;
	}
	
	/**
	 * Return Service Locator
	 * @implements \Zend\ServiceManager\ServiceLocatorAwareInterface
	 * @return ServiceLocatorInterface|NULL
	 */
	public function getServiceLocator() {
		return $this->serviceLocator;
	}
	
	/**
	 * Return instance of self
	 * @implements \Zend\ServiceManager\FactoryInterface
	 * @param ServiceLocatorInterface $serviceLocator
	 * @return ContactResponder $this
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {
		return $this;
	}
	
	/**
	 * Primary Callback. Uses config to hand off to other specialised methods
	 * @param EventInterface $e
	 * @return void
	 */
	public function respond(EventInterface $e) {
		if(null === $this->getServiceLocator()) {
			/**
			 * This is never likely to happen because the service manager will throw
			 * an exception when a call to $SL->get(__CLASS__) is made
			 */
			throw new Exception\DomainException('No service locator object has been injected. Has '.__CLASS__.' been added to service factories?');
		}
		$params = $e->getParams();
		if(!isset($params['form'])) {
			throw new Exception\InvalidArgumentException('The event passed to '.__METHOD__.' does not have a \'form\' parameter');
		}
		$sl = $this->getServiceLocator();
		$config = $sl->get('Config');
		if(!isset($config['netglue_contact'])) {
			return;
		}
		$config = $config['netglue_contact'];
		if(isset($config['responder']['email'])) {
			$this->sendMail($config['responder']['email'], $e);
		}
	}
	
	/**
	 * Send the message to the website owner and the auto responder if configured
	 * @param array $config
	 * @param EventInterface $e So we can get the params rom the triggered event
	 * @return void
	 */
	protected function sendMail(array $config, EventInterface $e) {
		if(true !== $config['enable']) {
			return;
		}
		/**
		 * Make Sure we've can get our template service and the Mail Transport Service
		 */
		$sl = $this->getServiceLocator();
		if(!$sl->has('MailTemplate')) {
			throw new Exception\DomainException('Cannot locate the Mail Template service which is required for sending messages. Make sure the module has been enabled in the main application config');
		}
		if(!$sl->has('MailTransport')) {
			throw new Exception\DomainException('No mail transport has been configured');
		}
		$mt = $sl->get('MailTemplate');
		$data = $e->getParams();
		// Array data to pass to the template
		$model = $data['form']->getData();
		$template = $config['template'];
		// Create website owner message and send it...
		$msg = $mt->createMessage($template, $model);
		$msg->addFrom(sprintf('%s <%s>', $model['fromName'], $model['email']))
			->addTo($config['recipient'])
			->setSubject($config['subject']);
		$tr = $sl->get('MailTransport');
		$tr->send($msg);
		// Create Receipt Email and send it
		if(true !== $config['send_receipt']) {
			return;
		}
		$template = $config['receipt_template'];
		$msg = $mt->createMessage($template, $model);
		$msg->addFrom($config['sender'])
			->addTo(sprintf('%s <%s>', $model['fromName'], $model['email']))
			->setSubject($config['receipt_subject']);
		$tr->send($msg);
	}
	
	
	
}