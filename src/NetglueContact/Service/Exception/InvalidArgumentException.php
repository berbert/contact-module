<?php
/**
 * Service Invalid Argument Exception
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @package	Netglue_ContactModule
 * @link https://bitbucket.org/netglue/zf2-contact-module
 */

namespace NetglueContact\Service\Exception;

/**
 * Service Invalid Argument Exception
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @package	Netglue_ContactModule
 * @link https://bitbucket.org/netglue/zf2-contact-module
 */
class InvalidArgumentException extends \InvalidArgumentException {
	
}