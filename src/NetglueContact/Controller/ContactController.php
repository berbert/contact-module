<?php
/**
 * Netglue Contact Module Action Controller
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @package	Netglue_ContactModule
 * @link https://bitbucket.org/netglue/zf2-contact-module
 */

namespace NetglueContact\Controller;

use NetglueContact\Form\ContactForm;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\Exception\RuntimeException as RuntimeException;

/**
 * Netglue Contact Module Action Controller
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @package	Netglue_ContactModule
 * @link https://bitbucket.org/netglue/zf2-contact-module
 */
class ContactController extends AbstractActionController {
	
	/**
	 * Successful contact form submission event
	 */
	const EVENT_CONTACT = 'netglue.contactController.success';
	
	/**
	 * Event called before rendering the form to allow for further form manipulation
	 */
	const EVENT_RENDER_FORM = 'netglue.contactController.renderForm';
	
	/**
	 * Contact Form
	 * @var ContactForm
	 */
	protected $_form;
	
	/**
	 * Assign Contact form to the view
	 * @return array
	 */
	public function indexAction() {
		
		$form = $this->getContactForm();
		
		/**
		 * Trigger the form render event
		 */
		$eventCenter = $this->getEventManager();
		$eventCenter->trigger(self::EVENT_RENDER_FORM, $this, array('form' => $form));
		
		return array(
			'form' => $form,
		);
	}
	
	/**
	 * Process the contact form
	 * @return void
	 */
	public function processAction() {
		if(!$this->request->isPost()) {
			return $this->redirect()->toRoute('contact_form');
		}
		$post = $this->request->getPost();
		$form = $this->getContactForm();
		$form->setData($post);
		if(!$form->isValid()) {
			$model = new ViewModel(array(
				'error' => true,
				'form'  => $form,
			));
			$model->setTemplate('netglue-contact/contact/index');
			return $model;
		}
		
		/**
		 * Raise the form submission event so listeners can do whatever
		 * needs to be done with the data submitted...
		 */
		$eventCenter = $this->getEventManager();
		$eventCenter->trigger(self::EVENT_CONTACT, $this, array(
			'form' => $form,
			'data' => $form->getData()
		));
		
		/**
		 * Redirect to thank you action
		 */
		return $this->redirect()->toRoute('contact_form/default', array('action' => 'thank-you'));
	}
	
	/**
	 * Render a thanks message
	 * @return void
	 */
	public function thankYouAction() {
		
	}
	
	/**
	 * Return contact form
	 * @return ContactForm $form
	 * @throws 
	 */
	public function getContactForm() {
		if($this->_form instanceof ContactForm) {
			$form = $this->_form;
			$form->prepare();
			$form->setAttribute('action', $this->url()->fromRoute('contact_form/default', array('action'=>'process')));
			$form->setAttribute('method', 'post');
			return $form;
		}
		throw new RuntimeException('Contact Form has not been injected into the controller');
	}
	
	/**
	 * Set the contact form to render in the view
	 * @param ContactForm $form
	 * @return void
	 */
	public function setContactForm(ContactForm $form) {
		$this->_form = $form;
	}
	
}